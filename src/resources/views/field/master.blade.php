<!DOCTYPE html>
<html>
<head>
    <title>Supaplex Editor</title>
    <meta charset="utf-8">

    <link rel="stylesheet" href="css/stylesheet.css">
</head>
<body>

@yield('content')

<script src="js/objects.js"></script>
<script src="js/editor.js"></script>
</body>
</html>